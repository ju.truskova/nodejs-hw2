const mongoose = require('mongoose');

const User = mongoose.model('User', {
  username: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
    unique: true,
  },
  createdDate: {
    type: String,
    required: false,
  },
});

module.exports = {
  User,
};
