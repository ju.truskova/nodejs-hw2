const mongoose = require('mongoose');

const noteSchema = mongoose.Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    default: false,
  },
  createdDate: {
    type: String,
    default: false,
  },
});

const Note = mongoose.model('note', noteSchema);

module.exports = {
  Note,
};
