/* eslint no-underscore-dangle: 0 */
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const { User } = require('../models/Users');

const registerUser = async (req, res) => {
  const { username, password } = req.body;

  const user = new User({
    username,
    password: await bcrypt.hash(password, 1),
    createdDate: new Date().toISOString(),
  });

  user
    .save()
    .then(() => res.json({ message: 'You registered successfully' }))
    .catch(() => {
      res.status(400).send({ message: 'You must enter name and password' });
    });
};

const loginUser = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });

  if (
    user
    && (await bcrypt.compare(String(req.body.password), String(user.password)))
  ) {
    const payload = { username: user.username, userId: user._id };
    const jwtToken = jwt.sign(payload, 'secret-jwt-key');

    return res.json({
      message: 'You logged in successfully',
      jwt_token: jwtToken,
    });
  }
  return res.status(400).json({ message: 'Not authorized' });
};

module.exports = {
  registerUser,
  loginUser,
};
