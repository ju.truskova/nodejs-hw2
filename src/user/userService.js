const bcrypt = require('bcryptjs');

const { User } = require('../models/Users');

const getUser = (req, res) => {
  const { userId } = req.user;

  User.findById(userId)
    .then((user) => res.json({ user }))
    .catch(() => {
      res.status(500).send({ message: 'Something went wrong...' });
    });
};

const deleteUser = (req, res) => {
  const { username, userId } = req.user;

  User.findByIdAndDelete(userId)
    .then(() => {
      res.json({ message: `bye, ${username}...:(` });
    })
    .catch(() => {
      res.status(500).send({ message: 'Something went wrong...' });
    });
};

const changePassword = async (req, res) => {
  const { userId } = req.user;
  const { oldPassword, newPassword } = req.body;
  const user = await User.findById(userId);

  if (
    user
    && (await bcrypt.compare(String(oldPassword), String(user.password)))
  ) {
    user.password = await bcrypt.hash(newPassword, 1);

    await user.save();

    return res.json({ message: 'Success' });
  }

  return res.status(400).send({ message: 'Something went wrong...' });
};

module.exports = {
  getUser,
  deleteUser,
  changePassword,
};
