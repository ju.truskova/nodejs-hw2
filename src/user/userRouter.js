const express = require('express');

const router = express.Router();

const { getUser, deleteUser, changePassword } = require('./userService');

const { authMiddleware } = require('../middleware/authMiddleware');

router.get('/me', authMiddleware, getUser);

router.delete('/me', authMiddleware, deleteUser);

router.patch('/me', authMiddleware, changePassword);

module.exports = {
  userRouter: router,
};
