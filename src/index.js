const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const { authRouter } = require('./auth/authRouter');
const { userRouter } = require('./user/userRouter');
const { notesRouter } = require('./notes/notesRouter');

const app = express();

mongoose.connect(
  'mongodb+srv://JuuTruskova:MTWZ0smf1nUdjZTU@yuliiatruskovamongodbcl.njldnpm.mongodb.net/usersNotes?retryWrites=true&w=majority',
);

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', userRouter);
app.use('/api/notes', notesRouter);

const start = async () => {
  try {
    app.listen(8080);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();
