const { Note } = require('../models/Notes');

const createNote = (req, res) => {
  const { text } = req.body;
  const { userId } = req.user;

  const note = new Note({
    text,
    userId,
    createdDate: new Date().toISOString(),
  });

  note.save()
    .then(() => {
      res.json({ message: 'You added new note' });
    })
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
};

const getNote = (req, res) => {
  console.log(req.params.id);
  Note.findById(req.params.id)
    .then((note) => res.json({ note }))
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
};

const getNotes = (req, res) => {
  console.log(req.query);
  Note.find({ userId: req.user.userId }, null, { skip: req.query.offset, limit: req.query.limit })
    .then((result) => {
      res.json({
        offset: req.query.offset,
        limit: req.query.limit,
        count: result.length,
        notes: result,
      });
    })
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
};

const deleteNote = (req, res) => {
  Note.findByIdAndDelete(req.params.id)
    .then(() => {
      res.json({ message: 'You deleted this note' });
    })
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
};

const completeNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  note.completed = !note.completed;

  return note.save()
    .then(() => res.json({ message: 'You updated this note' }))
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
};

const updateNote = async (req, res) => {
  const note = await Note.findById(req.params.id);
  const { text } = req.body;

  if (text) note.text = text;

  return note.save()
    .then(() => res.json({ message: 'You updated this note' }))
    .catch(() => {
      res.status(400).send({ message: 'Something went wrong...' });
    });
};

module.exports = {
  createNote,
  getNote,
  getNotes,
  updateNote,
  completeNote,
  deleteNote,
};
