const express = require('express');

const router = express.Router();

const {
  createNote,
  getNote,
  getNotes,
  updateNote,
  completeNote,
  deleteNote,
} = require('./notesService');

const { authMiddleware } = require('../middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes);

router.get('/:id', authMiddleware, getNote);

router.put('/:id', authMiddleware, updateNote);

router.patch('/:id', authMiddleware, completeNote);

router.delete('/:id', authMiddleware, deleteNote);

module.exports = {
  notesRouter: router,
};
